﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    interface IProduct:ICatalog
    {
        int Price { get; set; }
    }

}
