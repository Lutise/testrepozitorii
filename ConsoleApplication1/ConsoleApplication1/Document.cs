﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Document : IDocument
    {

        public void ProvodkaDocument()
        { Console.WriteLine("Документ проведён"); }

        public Document(int id, DateTime date)
        {
            ID=id;
            Date=date;
        }

        public int ID
        {
            get; set;
        }

        public DateTime Date
        {
            get; set;
        }
    }
}
