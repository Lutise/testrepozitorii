﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    interface IDocument 
    {
            int ID { get; set;  }
            DateTime Date { get; set; }
            void ProvodkaDocument();
    }

}
