﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class CatalogProduct : IProduct
        
    {  
 
        public CatalogProduct(int id, string name, int price)
        {
            ID=id;
            Name=name;
            Price=price;
        }

        public int ID
        {
            get;set;
        }

        public string Name
        {
            get;set;
        }
        public int Price
        {
            get; set;
        }
    }
}
